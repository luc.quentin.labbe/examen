﻿using ProperT.Core.Domain;
using ProperT.Core.Repositories;

namespace ProperT.MockingLibraries;

public class InMemoryPropertyRepository : IPropertyRepository
{
    private readonly Dictionary<Guid, Property> storage = new();

    public Task<Property?> GetOrDefault(Guid id)
    {
        storage.TryGetValue(id, out var result);
        return Task.FromResult(result);
    }

    public Task Save(Property property)
    {
        storage[property.Id] = property;
        return Task.CompletedTask;
    }

    public Task<Property?> Find(string address)
    {
        var result = storage.Values.FirstOrDefault(
            property => property.Address == address);
        return Task.FromResult(result);
    }
}
