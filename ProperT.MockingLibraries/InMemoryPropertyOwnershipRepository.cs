﻿using ProperT.Core.Gateways;
using ProperT.Core.Repositories;

namespace ProperT.MockingLibraries;

public class InMemoryPropertyOwnershipRepository : IPropertyOwnershipRepository
{
    private readonly Dictionary<string, PropertyOwnershipDto> storage = new();

    public Task<PropertyOwnershipDto?> GetLatest(string propertyAddress)
    {
        storage.TryGetValue(propertyAddress, out var result);
        return Task.FromResult(result);
    }

    public Task Publish(PropertyOwnershipDto newOwnership)
    {
        storage[newOwnership.Address] = newOwnership;
        return Task.CompletedTask;
    }
}

