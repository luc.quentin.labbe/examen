﻿using ProperT.Core.Domain.Common;
using ProperT.Core.Repositories;

namespace ProperT.MockingLibraries;

public class InMemoryIdentityRepository : IIdentityRepository
{
    private readonly Dictionary<Guid, Identity> storage = new();

    public Task<Identity?> GetOrDefault(Guid id)
    {
        storage.TryGetValue(id, out var result);
        return Task.FromResult(result);
    }

    public Task Save(Identity identity)
    {
        storage[identity.Id] = identity;
        return Task.CompletedTask;
    }
}
