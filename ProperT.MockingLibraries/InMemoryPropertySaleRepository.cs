﻿using ProperT.Core.Domain;
using ProperT.Core.Repositories;

namespace ProperT.MockingLibraries;

public class InMemoryPropertySaleRepository : IPropertySaleRepository
{
    private readonly Dictionary<Guid, PropertySale> storage = new();

    public Task<PropertySale?> GetOrDefault(Guid id)
    {
        storage.TryGetValue(id, out var result);
        return Task.FromResult(result);
    }

    public Task Save(PropertySale propertySale)
    {
        storage[propertySale.Id] = propertySale;
        return Task.CompletedTask;
    }

    public Task<IEnumerable<PropertySale>> FindForProperty(Guid propertyId)
    {
        var result = storage.Values.Where(
            sale => sale.Property?.Id == propertyId);
        return Task.FromResult(result);
    }

    public Task<PropertySale?> GetFirstOrDefault()
    {
        if (storage.Count == 0)
            return Task.FromResult<PropertySale?>(null);
        return Task.FromResult((PropertySale?)storage.FirstOrDefault().Value);
    }
}
