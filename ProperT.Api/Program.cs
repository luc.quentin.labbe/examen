using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using ProperT.Api.Authentication;
using ProperT.Core.Applications;
using ProperT.Core.Gateways;
using ProperT.Core.Repositories;
using ProperT.MockingLibraries;
using System.Text.Json;
using System.Text.Json.Serialization;
using static System.Net.Mime.MediaTypeNames;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
    });
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAuthentication()
    .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("Basic", null);
builder.Services.AddAuthorization(options =>
{
    options.FallbackPolicy = new AuthorizationPolicyBuilder()
        .RequireAuthenticatedUser()
        .Build();
});

builder.Services.AddSingleton<IIdentityRepository, InMemoryIdentityRepository>();
builder.Services.AddSingleton<IPropertyOwnershipRepository, InMemoryPropertyOwnershipRepository>();
builder.Services.AddSingleton<IPropertyRepository, InMemoryPropertyRepository>();
builder.Services.AddSingleton<IPropertySaleRepository, InMemoryPropertySaleRepository>();

builder.Services.AddSingleton<IOwnershipGatewayService, OwnershipGatewayService>();

builder.Services.AddSingleton<BuyerApplication>();
builder.Services.AddSingleton<SellerApplication>();
builder.Services.AddSingleton<NotaryApplication>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();
app.UseExceptionHandler(exceptionHandlerApp =>
{
    exceptionHandlerApp.Run(async context =>
    {
        var exceptionHandlerPathFeature =
            context.Features.Get<IExceptionHandlerPathFeature>();

        if (exceptionHandlerPathFeature?.Error is InvalidOperationException)
        {
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            context.Response.ContentType = Text.Plain;

            await context.Response.WriteAsync(exceptionHandlerPathFeature.Error.Message);
        }
    });
});

app.MapControllers();

app.Run();

public partial class Program { }