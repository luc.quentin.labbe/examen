﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;
using ProperT.Core.Repositories;
using System.Net.Http.Headers;
using System.Text;

namespace ProperT.Api.Authentication;

public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
{
    private readonly IIdentityRepository identityRepository;

    public BasicAuthenticationHandler(
        IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock,
        IIdentityRepository identityRepository) : base(options, logger, encoder, clock)
    {
        this.identityRepository = identityRepository;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        var authorizationHeader = ReadAuthorizationHeader();
        if (authorizationHeader is null)
            return AuthenticateResult.Fail("Missing Authorization header");

        var token = ExtractToken(authorizationHeader);
        if (token is null)
            return AuthenticateResult.Fail("Malformed Authorization header");

        var userNameAndPassword = ExtractUsernameAndPassword(token);
        if (userNameAndPassword is null)
            return AuthenticateResult.Fail("Invalid Authorization parameter format");

        var ticket = await CreateTicketForUser(userNameAndPassword.Value.Username, userNameAndPassword.Value.Password);
        if (ticket is null)
            return AuthenticateResult.Fail($"Invalid credentials for {userNameAndPassword.Value.Username}");

        return AuthenticateResult.Success(ticket);
    }

    private string? ReadAuthorizationHeader()
        => Request.Headers.TryGetValue("Authorization", out var value)
            ? value.ToString()
            : null;

    private static string? ExtractToken(string authorizationHeader)
    {
        if (!AuthenticationHeaderValue.TryParse(authorizationHeader, out var parsedValue))
            return null;

        try
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(parsedValue.Parameter ?? string.Empty));
        }
        catch (FormatException)
        {
            return null;
        }
    }

    private static (string Username, string Password)? ExtractUsernameAndPassword(string token)
    {
        return token.Split(':', 2) switch
        {
            [var userName, var password] => (userName, password),
            _ => null
        };
    }

    private async Task<AuthenticationTicket?> CreateTicketForUser(string userName, string password)
    {
        if (!Guid.TryParse(userName, out var guid))
            return null;

        var identity = await identityRepository.GetOrDefault(guid);
        if (identity is null)
            return null;

        // TODO: Implement proper password checking
        if (password is null)
            return null;

        var claimIdentity = new ClaimsIdentity(Scheme.Name);
        claimIdentity.AddClaim(new(ClaimTypes.NameIdentifier, userName));
        claimIdentity.AddClaim(new(ClaimTypes.Name, userName));
        claimIdentity.AddClaim(new(ClaimTypes.GivenName, identity.FullName));
        if (identity.IsNotary)
            claimIdentity.AddClaim(new(ClaimTypes.Role, "notary"));

        return new(new ClaimsPrincipal(claimIdentity), Scheme.Name);
    }
}