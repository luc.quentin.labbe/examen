﻿using ProperT.Core.Domain;
using ProperT.Core.Domain.Common;

namespace ProperT.Api;

public record PropertySale(
    Guid Id, 
    Guid SellerId,
    Guid? NotaryId,
    Guid? BuyerId,
    string? Address,
    Price? Price,
    LoanRequirements? LoanRequirements,
    PropertySaleStatus Status,
    DateTime? AgreementDate,
    DateTime? RecordingDate,
    DateTime? SigningDate,
    decimal? AmountDue)
{
    public static PropertySale FromDomain(IReadOnlyPropertySale sale)
        => new(
              sale.Id,
              sale.Seller.Id,
              sale.Notary?.Id,
              sale.Buyer?.Id,
              sale.Property?.Address,
              sale.Price,
              sale.LoanRequirements,
              sale.Status,
              sale.AgreementDate,
              sale.RecordingDate,
              sale.SigningDate,
              sale.AmountDue);
}
