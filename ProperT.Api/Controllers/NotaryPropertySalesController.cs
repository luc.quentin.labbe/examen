﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProperT.Core.Applications;
using ProperT.Core.Domain;

namespace ProperT.Api.Controllers;

[Route("notaryApi/propertySales")]
[ApiController]
[Authorize(Roles = "notary")]
public class NotaryPropertySalesController : ControllerBase
{
    private readonly NotaryApplication notaryApplication;

    public NotaryPropertySalesController(NotaryApplication notaryApplication)
    {
        this.notaryApplication = notaryApplication;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<PropertySale>> Get(Guid id)
    {
        var sale = await notaryApplication.GetPropertySale(AuthenticatedUserId, id);

        if (sale == null)
            return NotFound();

        return PropertySale.FromDomain(sale);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<PropertySale>> Put(Guid id, PropertySale changes)
    {
        if (id != changes.Id)
            return BadRequest("Id in the body does not match id in the url");

        var sale = await notaryApplication.GetPropertySale(AuthenticatedUserId, id);
        if (sale == null)
            return NotFound();

        if (changes.Status is PropertySaleStatus.Signed)
        { 
            await notaryApplication.SignPropertySale(sale);
        }

        if (changes.Status == PropertySaleStatus.Recorded)
        {
            await notaryApplication.AcknoledgePublicRecording(sale);
        }

        return PropertySale.FromDomain(sale);
    }

    [HttpGet]
    public async Task<IEnumerable<string>> Get(string address)
    {
        return await notaryApplication.FindListOfOwnersForProperty(address);
    }

    public Guid AuthenticatedUserId
        => Guid.TryParse(User.Identity?.Name, out var id)
        ? id
        : throw new Exception("Id couldn't be parsed");
}
