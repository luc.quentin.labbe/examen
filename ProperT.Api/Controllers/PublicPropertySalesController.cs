﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProperT.Core.Applications;
using ProperT.Core.Domain;

namespace ProperT.Api.Controllers;

[Route("publicApi/propertySales")]
[ApiController]
[Authorize]
public class PublicPropertySalesController : ControllerBase
{
    private readonly SellerApplication sellerApplication;
    private readonly BuyerApplication buyerApplication;

    public PublicPropertySalesController(SellerApplication sellerApplication, BuyerApplication buyerApplication)
    {
        this.sellerApplication = sellerApplication;
        this.buyerApplication = buyerApplication;
    }

    [HttpPost]
    public async Task<PropertySale> Create(PropertySale? payload)
    {
        var draft = await sellerApplication.CreateDraft(AuthenticatedUserId);
        if (payload is not null)
            await UpdateSaleForSeller(draft, payload);
        return PropertySale.FromDomain(draft);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<PropertySale>> Get(Guid id)
    {
        var sale = (await sellerApplication.GetPropertySale(AuthenticatedUserId, id))
            ?? (await buyerApplication.GetPropertySale(AuthenticatedUserId, id));

        if (sale == null)
            return NotFound();

        return PropertySale.FromDomain(sale);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<PropertySale>> Put(Guid id, PropertySale changes)
    {
        if (id != changes.Id)
            return BadRequest("Id in the body does not match id in the url");

        return (await TryPutForSeller())
            ?? (await TryPutForBuyer())
            ?? NotFound();

        async Task<ActionResult<PropertySale>?> TryPutForSeller()
        {
            var sale = await sellerApplication.GetPropertySale(AuthenticatedUserId, id);
            if (sale == null)
                return null;

            await UpdateSaleForSeller(sale, changes);
            return PropertySale.FromDomain(sale);
        }

        async Task<ActionResult<PropertySale>?> TryPutForBuyer()
        {
            var sale = await buyerApplication.GetPropertySale(AuthenticatedUserId, id);
            if (sale == null)
                return null;

            await UpdateSaleForBuyer(sale, changes);
            return PropertySale.FromDomain(sale);
        }
    }

    private Task UpdateSaleForSeller(IReadOnlyPropertySale sale, PropertySale payload)
        => sale.Status switch
        {
            PropertySaleStatus.Draft => UpdateDraftForSeller(sale, payload),
            PropertySaleStatus.Submitted => UpdateSubmittedForSeller(sale, payload),
            _ => throw new InvalidOperationException("Changes are not allowed for sales in status " + sale.Status),
        };

    private async Task UpdateDraftForSeller(IReadOnlyPropertySale draft, PropertySale payload)
    {
        if (payload.Address != null)
            await sellerApplication.IdentifyProperty(draft, payload.Address);
        if (payload.Price != null)
            await sellerApplication.SetPrice(draft, payload.Price);
        if (payload.NotaryId != null)
            await sellerApplication.AssignToNotary(draft, payload.NotaryId.Value);
        if (payload.Status == PropertySaleStatus.Submitted)
            await sellerApplication.Submit(draft);
        await UpdateSubmittedForSeller(draft, payload);
    }

    private async Task UpdateSubmittedForSeller(IReadOnlyPropertySale sale, PropertySale payload)
    {
        if (payload.BuyerId is not null)
            await sellerApplication.ProposeToBuyer(sale, payload.BuyerId.Value);
    }

    private Task UpdateSaleForBuyer(IReadOnlyPropertySale sale, PropertySale payload)
        => sale.Status switch
        {
            PropertySaleStatus.Submitted => UpdateSubmittedForBuyer(sale, payload),
            PropertySaleStatus.Agreed => UpdateAgreedForBuyer(sale, payload),
            _ => throw new InvalidOperationException("Changes are not allowed for sales in status " + sale.Status),
        };

    private async Task UpdateSubmittedForBuyer(IReadOnlyPropertySale sale, PropertySale payload)
    {
        if (payload.LoanRequirements is (var amount, var rate, var durationInMonths))
            await buyerApplication.AddLoadRequirement(sale, amount, rate, durationInMonths);
        if (payload.Status == PropertySaleStatus.Agreed)
            await buyerApplication.AgreeToPropertySale(sale);
    }

    private async Task UpdateAgreedForBuyer(IReadOnlyPropertySale sale, PropertySale payload)
    {
        if (payload.Status == PropertySaleStatus.Cancelled)
            await buyerApplication.CancelPropertySale(sale);
    }

    [HttpPost("{id}/resubmit")]
    public async Task<ActionResult<PropertySale>> Resubmit(Guid id)
    {
        var sale = await sellerApplication.GetPropertySale(AuthenticatedUserId, id);
        if (sale == null)
            return NotFound();

        var newSale = await sellerApplication.SubmitNewSaleFromCancelled(sale);
        return CreatedAtAction(
            nameof(Get), 
            new { id = newSale.Id },
            PropertySale.FromDomain(newSale));
    }

    public Guid AuthenticatedUserId
        => Guid.TryParse(User.Identity?.Name, out var id)
        ? id
        : throw new Exception("Id couldn't be parsed");
}
