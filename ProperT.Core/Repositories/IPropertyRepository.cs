﻿using ProperT.Core.Domain;

namespace ProperT.Core.Repositories;

public interface IPropertyRepository
{
    /// <summary>
    /// Renvoit la propriété demandée, ou null si elle n'est pas trouvée. 
    /// </summary>
    Task<Property?> GetOrDefault(Guid id);

    /// <summary>
    /// Sauvegarde l'état de l'identité, la mettant à jour si elle existe déjà(via son id), 
    /// ou la créant sinon.
    /// </summary>
    Task Save(Property property);

    /// <summary>
    /// Renvoit une propriété dont l'addresse est identique à celle en paramètre, 
    /// ou null si elle n'est pas trouvée. 
    /// </summary>
    Task<Property?> Find(string address);
}
