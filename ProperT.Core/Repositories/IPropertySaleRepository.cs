﻿using ProperT.Core.Domain;

namespace ProperT.Core.Repositories;

public interface IPropertySaleRepository
{
    /// <summary>
    /// Renvoit la transaction demandée, 
    /// ou null si elle n'est pas trouvée. 
    /// </summary>
    Task<PropertySale?> GetOrDefault(Guid id);

    /// <summary>
    /// Renvoit la première transaction, 
    /// ou null si il n'y en a pas. 
    /// </summary>
    Task<PropertySale?> GetFirstOrDefault();

    /// <summary>
    /// Renvoit toutes les transactions demandées concernant une propriété donnée.
    /// </summary>
    Task<IEnumerable<PropertySale>> FindForProperty(Guid propertyId);

    /// <summary>
    /// Sauvegarde l'état de la transaction, la mettant à jour si elle existe déjà(via son id), 
    /// ou la créant sinon.
    /// </summary>
    Task Save(PropertySale propertySale);
}
