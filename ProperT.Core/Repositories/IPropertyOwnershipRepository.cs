﻿using ProperT.Core.Gateways;

namespace ProperT.Core.Repositories;

public interface IPropertyOwnershipRepository
{
    /// <summary>
    /// Renvoit le dernier état d'appartenance de la propriété dont l'addresse est identique à celle passée en paramètre,
    /// ou null en l'absence d'information.
    /// </summary>
    Task<PropertyOwnershipDto?> GetLatest(string propertyAddress);

    /// <summary>
    /// Ajoute un nouvel état à l'historique des états d'appartenances de la propriété dont l'addresse est mentionné dans l'objet.
    /// </summary>
    Task Publish(PropertyOwnershipDto newOwnership);
}
