﻿using ProperT.Core.Domain.Common;

namespace ProperT.Core.Repositories
{
    public interface IIdentityRepository
    {
        /// <summary>
        /// Renvoit l'identité demandée, ou null si elle n'est pas trouvée.
        /// </summary>
        Task<Identity?> GetOrDefault(Guid id);

        /// <summary>
        /// Sauvegarde l'état de l'identité, la mettant à jour si elle existe déjà(via son id), ou la créant sinon.
        /// </summary>
        Task Save(Identity identity);
    }
}
