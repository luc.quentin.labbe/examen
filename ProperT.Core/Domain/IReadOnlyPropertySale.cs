﻿using ProperT.Core.Domain.Common;

namespace ProperT.Core.Domain;

public interface IReadOnlyPropertySale
{
    Guid Id { get; }

    IReadOnlyIdentity Seller { get; }
    IReadOnlyIdentity? Buyer { get; }
    IReadOnlyIdentity? Notary { get; }

    IReadOnlyProperty? Property { get; }
    PropertySaleStatus Status { get; }

    Price? Price { get; }
    decimal SecurityDeposit { get; }
    decimal AmountDue { get; }

    LoanRequirements? LoanRequirements { get; }

    DateTime? AgreementDate { get; }
    DateTime? RecordingDate { get; }
    DateTime? SigningDate { get; }
}
