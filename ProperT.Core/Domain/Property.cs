﻿namespace ProperT.Core.Domain;

public record Property(Guid Id, string Address) : IReadOnlyProperty
{
    public Property(string address)
        : this(Guid.NewGuid(), address)
    { }
}
