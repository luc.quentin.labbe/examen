﻿namespace ProperT.Core.Domain
{
    public enum PropertySaleStatus
    {
        Draft,
        Submitted,
        Agreed,
        Cancelled,
        Signed,
        Recorded
    }
}
