﻿namespace ProperT.Core.Domain.Common;

public record Price(decimal Amount, string Currency);
