﻿namespace ProperT.Core.Domain.Common;

public interface IReadOnlyIdentity
{
    Guid Id { get; }
    string? FirstName { get; }
    string? LastName { get; }
}
