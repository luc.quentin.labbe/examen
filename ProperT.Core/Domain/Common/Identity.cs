﻿namespace ProperT.Core.Domain.Common;

public class Identity : IReadOnlyIdentity
{
    public Guid Id { get; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public bool IsNotary { get; set; }

    public string FullName => $"{LastName?.ToUpper()}, {FirstName}";

    public Identity(Guid id)
    {
        Id = id;
    }

    public override bool Equals(object? obj)
        => obj is Identity other
        && other.Id == Id;

    public override int GetHashCode()
        => Id.GetHashCode();

    public override string ToString()
        => $" Id {Id}: FirstName = \"{FirstName}\"; LastName = \"{LastName}\"";
}
