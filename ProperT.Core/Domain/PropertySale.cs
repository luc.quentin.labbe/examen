﻿using ProperT.Core.Domain.Common;
using System.Diagnostics.CodeAnalysis;

namespace ProperT.Core.Domain;

// On utilise maintenant SetProperty pour éviter les redondances
public class PropertySale : IReadOnlyPropertySale
{
    public Guid Id { get; }
    public Identity Seller { get; }
    IReadOnlyIdentity IReadOnlyPropertySale.Seller => Seller;

    public PropertySale(Identity seller)
        : this(Guid.NewGuid(), seller)
    { }

    public PropertySale(Guid id, Identity seller)
    {
        Id = id;
        Seller = seller;
    }

    public PropertySaleStatus Status { get; private set; } = PropertySaleStatus.Draft;

    private Property? property;
    public Property? Property
    {
        get => property;
        set => SetProperty(ref property, value, PropertySaleStatus.Draft);
    }
    IReadOnlyProperty? IReadOnlyPropertySale.Property => Property;

    private Identity? notary;
    public Identity? Notary
    {
        get => notary;
        set => SetProperty(ref notary, value, PropertySaleStatus.Draft);
    }
    IReadOnlyIdentity? IReadOnlyPropertySale.Notary => Notary;

    private Identity? buyer;
    public Identity? Buyer
    {
        get => buyer;
        set => SetProperty(ref buyer, value, PropertySaleStatus.Submitted);
    }
    IReadOnlyIdentity? IReadOnlyPropertySale.Buyer => Buyer;

    private Price? price;
    public Price? Price
    {
        get => price;
        set => SetProperty(ref price, value, PropertySaleStatus.Draft);
    }

    public decimal SecurityDeposit => (price?.Amount ?? 0m) * 0.1m;
    public decimal AmountDue { get; private set; }

    public DateTime? AgreementDate { get; private set; }
    public DateTime? RecordingDate { get; private set; }
    public DateTime? SigningDate { get; private set; }

    private LoanRequirements? loanRequirements;
    public LoanRequirements? LoanRequirements
    {
        get => loanRequirements;
        set => SetProperty(ref loanRequirements, value, PropertySaleStatus.Submitted);
    }

    private void SetProperty<T>(ref T field, T value, PropertySaleStatus validStatus)
    {
        if (Status != validStatus)
            throw new InvalidOperationException($"Can't change this property because status is {Status} instead of {validStatus}");

        field = value;
    }

    public void Submit()
    {
        if (price == null || Property == null || Notary == null)
            throw new InvalidOperationException($"Can't submit sale {Id} because some required properties are missing");

        Status = PropertySaleStatus.Submitted;
        AmountDue = SecurityDeposit;
    }

    public void RecordAgreement(DateTime agreementDate)
    {
        if (Status != PropertySaleStatus.Submitted)
            throw new InvalidOperationException($"Can't record agreement for sale {Id} because status is {Status} instead of Submitted");

        Status = PropertySaleStatus.Agreed;
        AgreementDate = agreementDate;
    }

    public void Cancel(DateTime cancellationDate)
    {
        if (Status != PropertySaleStatus.Agreed)
            throw new InvalidOperationException($"Can't cancel sale {Id} because status is {Status} instead of Agreed");

        Status = PropertySaleStatus.Cancelled;
        if (cancellationDate - AgreementDate <= TimeSpan.FromDays(31))
            AmountDue = 0m;
    }

    public void RecordSignature(DateTime signingDate)
    {
        if (Status != PropertySaleStatus.Agreed)
            throw new InvalidOperationException($"Can't record signature agreement for sale {Id} because status is invalid");

        Status = PropertySaleStatus.Signed;
        SigningDate = signingDate;
        AmountDue = price!.Amount; // Price cannot be null in this status
    }

    public void AcknowledgePublicRecording(DateTime recordingDate)
    {
        if (Status != PropertySaleStatus.Signed)
            throw new InvalidOperationException($"Can't acknowledge public recording for sale {Id} because status is {Status} instead of Signed");

        Status = PropertySaleStatus.Recorded;
        RecordingDate = recordingDate;
    }
}
