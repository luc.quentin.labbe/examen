﻿namespace ProperT.Core.Gateways;

public record PropertyOwnershipDto(string Address, string Owner, bool IsTransferring);
