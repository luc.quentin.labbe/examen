﻿using ProperT.Core.Domain;

namespace ProperT.Core.Gateways;

public interface IOwnershipGatewayService
{
    Task EnsurePropertyIsOwnedBySeller(PropertySale sale, bool ensureNotTransferring);
    Task PublishStatusUpdate(PropertySale sale);
}
