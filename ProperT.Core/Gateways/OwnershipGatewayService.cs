﻿using ProperT.Core.Domain;
using ProperT.Core.Repositories;

namespace ProperT.Core.Gateways;

public class OwnershipGatewayService : IOwnershipGatewayService
{
    private readonly IPropertyOwnershipRepository propertyOwnerhipRepository;
    public OwnershipGatewayService(IPropertyOwnershipRepository propertyOwnerhipRepository)
    {
        this.propertyOwnerhipRepository = propertyOwnerhipRepository;
    }

    public async Task EnsurePropertyIsOwnedBySeller(PropertySale sale, bool ensureNotTransferring)
    {
        if (sale.Property == null)
            return;

        var ownership = await propertyOwnerhipRepository.GetLatest(sale.Property.Address);

        if (ownership == null)
            return;

        if (ownership.Owner != sale.Seller.FullName)
            throw new InvalidOperationException($"Cannot submit sale proposal of {sale.Id} because "
            + $"it does not belong to {sale.Seller.FullName} but to {ownership.Owner}");

        if (ensureNotTransferring && ownership.IsTransferring)
            throw new InvalidOperationException($"Cannot submit sale proposal of {sale.Id} because "
            + $"another proposal is ongoing");
    }

    public async Task PublishStatusUpdate(PropertySale sale)
    {
        if (sale.Property is null)
            return;

        // Protection: le système externe ne supporte pas les adresses nulles, mais je ne suis pas certain que toutes les propriétés aient une adresse à ce stade
        //var address = sale.Property.Address ?? "<<unknown>>";
        //Maintenant le développeur gère aussi les cas "" et " " pour éviter des adresses nulles.
        var address = string.IsNullOrWhiteSpace(sale.Property.Address) ? "<<unknown>>" : sale.Property.Address;

        await propertyOwnershipRepository.Publish(new PropertyOwnershipDto(
            address,
            GetOwnerFullName(sale),
            IsTransferring: sale.Status switch
            {
                PropertySaleStatus.Submitted => true,
                PropertySaleStatus.Agreed => true,
                _ => false
            }));
    }


    private static string GetOwnerFullName(PropertySale sale)
    {
        if (sale.Status < PropertySaleStatus.Signed)
            return sale.Seller.FullName;
        else
            return sale.Buyer!.FullName; // Buyer cannot be null after signature
    }
}
