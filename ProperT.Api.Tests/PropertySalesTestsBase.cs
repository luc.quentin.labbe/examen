﻿using Microsoft.AspNetCore.Mvc.Testing;
using ProperT.Core.Domain;
using ProperT.Core.Domain.Common;
using ProperT.Core.Gateways;
using ProperT.Core.Repositories;
using ProperT.MockingLibraries;
using System.Text;

namespace ProperT.Api.Tests;

public class PropertySalesTestsBase : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> factory;

    protected readonly InMemoryIdentityRepository identityRepository = new();
    protected readonly InMemoryPropertyOwnershipRepository propertyOwnershipRepository = new();
    protected readonly InMemoryPropertyRepository propertyRepository = new();
    protected readonly InMemoryPropertySaleRepository propertySaleRepository = new();

    protected readonly Identity sellerIdentity;
    protected readonly Identity buyerIdentity;
    protected readonly Identity notaryIdentity;

    protected readonly HttpClient sellerClient;
    protected readonly HttpClient buyerClient;
    protected readonly HttpClient notaryClient;

    protected Guid sellerGuid = Guid.Parse("505734f4-a80a-406c-b8a1-3c1b11ff3a15");
    protected Guid buyerGuid = Guid.Parse("00d6cb36-3ba9-416b-922a-9f21676a6c24");
    protected Guid notaryGuid = Guid.Parse("8b339ade-5b17-471d-99d2-31c82d10298f");

    protected const string propertyAddress = "72 rue de la République, 92150 Suresnes, Appartement 42";
    protected const string propertyAddress2 = "42 rue de la réponse, 95420 Magny-en-vexin, Appartement 666";

    public PropertySalesTestsBase(WebApplicationFactory<Program> factory)
    {
        sellerIdentity = new(sellerGuid) { FirstName = "Jean", LastName = "Vent" };
        buyerIdentity = new(buyerGuid) { FirstName = "Jacques", LastName = "Chett" };
        notaryIdentity = new(notaryGuid) { FirstName = "Guy", LastName = "Clark", IsNotary = true };

        identityRepository.Save(sellerIdentity);
        identityRepository.Save(buyerIdentity);
        identityRepository.Save(notaryIdentity);

        this.factory = factory.WithWebHostBuilder(options =>
        {
            options.ConfigureServices(services =>
            {
                services.AddSingleton<IIdentityRepository>(identityRepository);
                services.AddSingleton<IPropertyOwnershipRepository>(propertyOwnershipRepository);
                services.AddSingleton<IPropertyRepository>(propertyRepository);
                services.AddSingleton<IPropertySaleRepository>(propertySaleRepository);
            });
        });
        sellerClient = CreateClient(sellerGuid);
        notaryClient = CreateClient(notaryGuid);
        buyerClient = CreateClient(buyerGuid);
    }

    protected HttpClient CreateClient(Guid userGuid)
    {
        var client = factory.CreateClient();
        client.DefaultRequestHeaders.Authorization = new("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{userGuid}:")));

        return client;
    }

    protected async Task AssertSuccessAsync(HttpResponseMessage response)
    {
        if (!response.IsSuccessStatusCode)
            Assert.Fail(response.StatusCode + " - " + await response.Content.ReadAsStringAsync());
    }

    protected async Task AssertGetReturnsSame(HttpClient client, PropertySale propertySale)
        => Assert.Equal(await GetPropertySale(client, propertySale), propertySale);

    protected async Task<PropertySale?> GetPropertySale(HttpClient client, PropertySale propertySale)
        => await client.GetFromJsonAsync<PropertySale>($"publicApi/propertySales/{propertySale.Id}");

    protected async Task<PropertySale> PutPropertySale(HttpClient client, PropertySale propertySale)
    {
        var putResponse = await client.PutAsJsonAsync($"publicApi/propertySales/{propertySale.Id}", propertySale);
        await AssertSuccessAsync(putResponse);

        var modificationResult = await putResponse.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(modificationResult);
        return modificationResult;
    }

    protected async Task<Core.Domain.PropertySale> SetupSale(string propertyAddress, string propertyGuidStr, string saleGuidStr)
    {
        var propertyGuid = Guid.Parse(propertyGuidStr);
        var property = new Property(propertyGuid, propertyAddress);
        await propertyRepository.Save(property);

        var saleGuid = Guid.Parse(saleGuidStr);
        var sale = new Core.Domain.PropertySale(saleGuid, sellerIdentity)
        {
            Property = property,
            Notary = notaryIdentity,
            Price = new(195000, "EUR"),
        };
        sale.Submit();
        await propertySaleRepository.Save(sale);
        return sale;
    }

    protected async Task<Guid> SetupSignedSale(bool assert)
    {
        await propertyOwnershipRepository.Publish(new("16 rue des Peupliers, Seignosse", "GOLD, Mary", true));
        Guid saleGuid = await SetupAgreedSale(120);
        var response = await notaryClient.PutAsJsonAsync($"notaryApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "signed"
        });
        if(assert)
            await AssertSigned(response);
        return saleGuid;
    }

    protected async Task AssertSigned(HttpResponseMessage response)
    {
        await AssertSuccessAsync(response);

        var result = await response.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(result);
        Assert.Equal("signed", result.Status);
        Assert.Equal(195000, result.AmountDue);

        Assert.NotNull(result.SigningDate);
        Assert.InRange(result.SigningDate.Value, DateTime.Today, DateTime.Today.AddDays(1));
        Assert.Null(result.RecordingDate);

        await AssertOwnershipUpdate("CHETT, Jacques", false);
    }

    protected async Task<Guid> SetupAgreedSale(int saleAgeInDays)
    {
        var sale = await SetupSale(propertyAddress, "9622c257-198b-4f7e-8c83-22a34f7afa98", "c99ed312-c878-42f3-9e0f-9745c1cf9786");
        sale.Buyer = buyerIdentity;
        sale.LoanRequirements = new(175000, 0.02m, 20);
        sale.RecordAgreement(DateTime.Today.AddDays(-saleAgeInDays));
        await propertySaleRepository.Save(sale);
        return sale.Id;
    }

    protected async Task AssertOwnershipUpdate(string owner, bool isTransfering)
    {
        var ownership = await propertyOwnershipRepository.GetLatest(propertyAddress);
        Assert.Equal(new PropertyOwnershipDto(propertyAddress, owner, isTransfering), ownership);
    }
}