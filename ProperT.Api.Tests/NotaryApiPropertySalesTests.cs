using Microsoft.AspNetCore.Mvc.Testing;
using ProperT.Core.Domain;
using ProperT.Core.Domain.Common;
using ProperT.Core.Gateways;
using ProperT.Core.Repositories;
using System.Net;

namespace ProperT.Api.Tests;

public class NotaryApiPropertySalesTests : PropertySalesTestsBase
{
    public NotaryApiPropertySalesTests(WebApplicationFactory<Program> factory)
        : base(factory)
    {
    }

    [Fact]
    public async Task CanSignSaleIfNotOwned()
    {
        await SetupSale(propertyAddress2, "641374d5-eb1d-42f6-a60b-6dabdc4666f0", "b72d3ae7-2f67-4728-b6b1-638426be14f0");
        await SetupSignedSale(true);
    }

    [Fact]
    public async Task<Guid> CanSignSaleIfOwnedBySellerAndNotTransfering()
    {
        await propertyOwnershipRepository.Publish(new(propertyAddress, "VENT, Jean", false));
        Guid saleGuid = await SetupAgreedSale(120);
        var response = await notaryClient.PutAsJsonAsync($"notaryApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "signed"
        });
        await AssertSigned(response);

        return saleGuid;
    }

    [Fact]
    public async Task<Guid> CanSignSaleIfOwnedBySellerAndTransfering()
    {
        await propertyOwnershipRepository.Publish(new(propertyAddress, "VENT, Jean", true));
        Guid saleGuid = await SetupAgreedSale(120);
        var response = await notaryClient.PutAsJsonAsync($"notaryApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "signed"
        });
        await AssertSigned(response);

        return saleGuid;
    }

    [Fact]
    public async Task<Guid> CannotSignSaleIfOwnedBySomeoneElse()
    {
        await propertyOwnershipRepository.Publish(new(propertyAddress, "GOLD, Mary", false));
        Guid saleGuid = await SetupAgreedSale(120);
        var response = await notaryClient.PutAsJsonAsync($"notaryApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "signed"
        });
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

        return saleGuid;
    }

    [Fact]
    public async Task<Guid> CanRecordSignedSale()
    {
        Guid saleGuid = await SetupSignedSale(false);
        var response = await notaryClient.PutAsJsonAsync($"notaryApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "recorded"
        });
        await AssertSuccessAsync(response);

        var result = await response.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(result);
        Assert.Equal("recorded", result.Status);
        Assert.Equal(195000, result.AmountDue);

        Assert.NotNull(result.SigningDate);
        Assert.InRange(result.SigningDate.Value, DateTime.Today, DateTime.Today.AddDays(1));
        Assert.NotNull(result.RecordingDate);
        Assert.InRange(result.RecordingDate.Value, DateTime.Today, DateTime.Today.AddDays(1));

        return saleGuid;
    }

    [Fact]
    public async Task CanLookupOwners()
    {
        await SetupSaleHistory();

        var result = await notaryClient.GetFromJsonAsync<string[]>($"notaryApi/propertySales?address={Uri.EscapeDataString(propertyAddress)}");
        Assert.NotNull(result);
        Assert.Equal(new[]
        {
            "VENT, Jean",
            "SMITH, Jane",
            "DOE, John"
        }, result);
    }

    [Fact]
    public async Task NonNotaryCannotAccessNotaryApi()
    {
        await SetupSaleHistory();

        var response = await buyerClient.GetAsync($"notaryApi/propertySales?address={Uri.EscapeDataString(propertyAddress)}");
        Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
    }

    protected async Task SetupSaleHistory()
    {
        var propertyGuid = Guid.Parse("74529704-a81b-4993-ace8-fbff6d5a137d");
        var property = new Property(propertyGuid, propertyAddress);
        await propertyRepository.Save(property);


        var firstOwner = new Identity(Guid.NewGuid()) { FirstName = "John", LastName = "Doe" };
        var secondOwner = new Identity(Guid.NewGuid()) { FirstName = "Jane", LastName = "Smith" };
        var almostThirdOwner = new Identity(Guid.NewGuid()) { FirstName = "Mark", LastName = "Wilson" };

        await identityRepository.Save(firstOwner);
        await identityRepository.Save(secondOwner);
        await identityRepository.Save(almostThirdOwner);

        await RecordSale(firstOwner, secondOwner, 120000, 5, false);
        await RecordSale(secondOwner, almostThirdOwner, 160000, 4, true);
        await RecordSale(secondOwner, sellerIdentity, 160000, 3, false);
        await RecordSale(sellerIdentity, buyerIdentity, 195000, 0, null);

        async Task RecordSale(Identity seller, Identity buyer, decimal price, int ageInYears, bool? cancel)
        {
            var sale = new Core.Domain.PropertySale(Guid.NewGuid(), seller)
            {
                Property = property,
                Notary = notaryIdentity,
                Price = new(price, "EUR"),
            };
            sale.Submit();
            sale.Buyer = buyer;
            sale.LoanRequirements = new(price - 10000, 0.02m, 20);
            sale.RecordAgreement(DateTime.Today.AddYears(-ageInYears));
            if (cancel == true)
            {
                sale.Cancel(DateTime.Today.AddYears(-ageInYears).AddMonths(1));
            }
            else if (cancel == false)
            {
                sale.RecordSignature(DateTime.Today.AddYears(-ageInYears).AddMonths(3));
                sale.AcknowledgePublicRecording(DateTime.Today.AddYears(-ageInYears).AddMonths(6));
            }
            await propertySaleRepository.Save(sale);
        }
    }
}