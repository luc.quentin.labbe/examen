using Microsoft.AspNetCore.Mvc.Testing;

namespace ProperT.Api.Tests;

public class PublicApiPropertySalesTests : PropertySalesTestsBase
{
    public PublicApiPropertySalesTests(WebApplicationFactory<Program> factory)
        : base(factory)
    {
    }

    [Fact]
    public async Task<PropertySale> CreateWithoutPayload()
    {
        var response = await sellerClient.PostAsync("publicApi/propertySales", null);
        await AssertSuccessAsync(response);

        var result = await response.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(result);
        Assert.NotEqual(result.Id, result.SellerId);
        Assert.Null(result.NotaryId);
        Assert.Null(result.BuyerId);
        Assert.Null(result.Address);
        Assert.Null(result.Price);
        Assert.Equal("draft", result.Status);

        return result;
    }

    [Fact]
    public async Task CreateThenGet()
    {
        var result = await CreateWithoutPayload();
        await AssertGetReturnsSame(sellerClient, result);
    }

    [Fact]
    public async Task SetAddress()
    {
        var creationResult = await CreateWithoutPayload();

        PropertySale? result = await PutPropertySale(sellerClient, creationResult with
        {
            Address = propertyAddress
        });
        Assert.Equal(propertyAddress, result.Address);
        Assert.NotNull(await propertyRepository.Find(propertyAddress));

        await AssertGetReturnsSame(sellerClient, result);
    }

    [Fact]
    public async Task SetPrice()
    {
        var creationResult = await CreateWithoutPayload();

        PropertySale? result = await PutPropertySale(sellerClient, creationResult with
        {
            Price = new Price(195000, "EUR")
        });
        Assert.Equal(new Price(195000, "EUR"), result.Price);

        await AssertGetReturnsSame(sellerClient, result);
    }

    [Fact]
    public async Task SetNotary()
    {
        var creationResult = await CreateWithoutPayload();

        PropertySale? result = await PutPropertySale(sellerClient, creationResult with
        {
            NotaryId = notaryGuid
        });
        Assert.Equal(notaryGuid, result.NotaryId);

        await AssertGetReturnsSame(sellerClient, result);
    }

    [Fact]
    public async Task SubmitInTwoSteps()
    {
        var creationResult = await CreateWithoutPayload();

        PropertySale? firstStepResult = await PutPropertySale(sellerClient, creationResult with
        {
            Address = propertyAddress,
            Price = new Price(195000, "EUR"),
            NotaryId = notaryGuid
        });
        Assert.Equal(propertyAddress, firstStepResult.Address);
        Assert.Equal(new Price(195000, "EUR"), firstStepResult.Price);
        Assert.Equal(notaryGuid, firstStepResult.NotaryId);
        Assert.Equal("draft", firstStepResult.Status);

        PropertySale? secondStepResult = await PutPropertySale(sellerClient, firstStepResult with
        {
            Status = "submitted"
        });
        Assert.Equal(propertyAddress, secondStepResult.Address);
        Assert.Equal(new Price(195000, "EUR"), secondStepResult.Price);
        Assert.Equal(notaryGuid, secondStepResult.NotaryId);
        Assert.Equal("submitted", secondStepResult.Status);

        await AssertOwnershipUpdate("VENT, Jean", true);
        await AssertGetReturnsSame(sellerClient, secondStepResult);
    }

    [Fact]
    public async Task SubmitInOneStep()
    {
        var response = await sellerClient.PostAsJsonAsync("publicApi/propertySales", new
        {
            Address = propertyAddress,
            Price = new Price(195000, "EUR"),
            NotaryId = notaryGuid,
            Status = "submitted"
        });
        await AssertSuccessAsync(response);

        var result = await response.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(result);
        Assert.NotEqual(result.Id, result.SellerId);
        Assert.Equal(propertyAddress, result.Address);
        Assert.Equal(new Price(195000, "EUR"), result.Price);
        Assert.Equal(notaryGuid, result.NotaryId);
        Assert.Equal("submitted", result.Status);

        await AssertOwnershipUpdate("VENT, Jean", true);
        await AssertGetReturnsSame(sellerClient, result);
    }

    [Fact]
    public async Task<PropertySale> SubmitOnPost()
    {
        var response = await sellerClient.PostAsJsonAsync("publicApi/propertySales", new
        {
            Address = propertyAddress,
            Price = new Price(195000, "EUR"),
            NotaryId = notaryGuid,
            Status = "submitted"
        });
        await AssertSuccessAsync(response);

        var result = await response.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(result);
        Assert.NotEqual(result.Id, result.SellerId);
        Assert.Equal(propertyAddress, result.Address);
        Assert.Equal(new Price(195000, "EUR"), result.Price);
        Assert.Equal(notaryGuid, result.NotaryId);
        Assert.Null(result.BuyerId);
        Assert.Equal("submitted", result.Status);

        await AssertOwnershipUpdate("VENT, Jean", true);
        await AssertGetReturnsSame(sellerClient, result);
        return result;
    }

    [Fact]
    public async Task<PropertySale> AssignBuyerOnSubmitted()
    {
        var submittedSale = await SubmitOnPost();

        PropertySale result = await PutPropertySale(sellerClient, submittedSale with
        {
            BuyerId = buyerGuid,
        });
        Assert.Equal(buyerGuid, result.BuyerId);

        await AssertGetReturnsSame(sellerClient, result);
        return result;
    }

    [Fact]
    public async Task<PropertySale> CannotAssignBuyerOnDraft()
    {
        var creationResult = await CreateWithoutPayload();

        PropertySale? firstEditionResult = await PutPropertySale(sellerClient, creationResult with
        {
            Address = propertyAddress,
            Price = new Price(195000, "EUR"),
            NotaryId = notaryGuid,
        });

        var putResponse = await sellerClient.PutAsJsonAsync($"publicApi/propertySales/{firstEditionResult.Id}", firstEditionResult with
        {
            BuyerId = buyerGuid,
        });
        Assert.False(putResponse.IsSuccessStatusCode);

        await AssertGetReturnsSame(sellerClient, firstEditionResult);
        return firstEditionResult;
    }

    [Fact]
    public async Task CannotChangeAddressAfterSubmission()
    {
        var submittedSale = await SubmitOnPost();
        
        PropertySale result = await PutPropertySale(sellerClient, submittedSale with
        {
            Address = "13 rue des Peupliers, Seignosse"
        });
        Assert.Equal(propertyAddress, result.Address);
    }

    [Fact]
    public async Task CannotChangePriceAfterSubmission()
    {
        var submittedSale = await SubmitOnPost();

        PropertySale result = await PutPropertySale(sellerClient, submittedSale with
        {
            Price = new Price(196000, "EUR")
        });
        Assert.Equal(195000, result.Price?.Amount);
    }

    [Fact]
    public async Task CannotChangeNotaryAfterSubmission()
    {
        var submittedSale = await SubmitOnPost();

        PropertySale? result = await PutPropertySale(sellerClient, submittedSale with
        {
            NotaryId = Guid.NewGuid()
        });
        Assert.Equal(notaryGuid, result.NotaryId);
    }

    [Fact]
    public async Task BuyerCanSetLoanRequirementOnAssignedSale()
    {
        var submittedSale = await AssignBuyerOnSubmitted();

        PropertySale? result = await PutPropertySale(buyerClient, submittedSale with
        {
            LoanRequirements = new(175000, 0.02m, 20)
        });
        Assert.Equal(new LoanRequirements(175000, 0.02m, 20), result.LoanRequirements);

        await AssertGetReturnsSame(sellerClient, result);
        await AssertGetReturnsSame(buyerClient, result);
    }

    [Fact]
    public async Task<PropertySale> BuyerCanAgreeOnAssignedSaleWithLoanRequirement()
    {
        var submittedSale = await AssignBuyerOnSubmitted();

        PropertySale? result = await PutPropertySale(buyerClient, submittedSale with
        {
            LoanRequirements = new(175000, 0.02m, 20),
            Status = "agreed",
            AgreementDate = DateTime.Today.AddDays(-1) // should be ignored
        });
        Assert.Equal(new LoanRequirements(175000, 0.02m, 20), result.LoanRequirements);
        Assert.Equal("agreed", result.Status);
        Assert.NotNull(result.AgreementDate);
        Assert.InRange(result.AgreementDate.Value, DateTime.Today, DateTime.Today.AddDays(1));
        Assert.Equal(19500m, result.AmountDue);

        await AssertOwnershipUpdate("VENT, Jean", true);
        await AssertGetReturnsSame(sellerClient, result);
        await AssertGetReturnsSame(buyerClient, result);

        return result;
    }

    [Theory]
    [InlineData(0, 0)]
    [InlineData(30, 0)]
    [InlineData(31, 19500)]
    public async Task<Guid> BuyerCanCancelSaleRightAfterAgreement(int saleAgeInDays, decimal expectedAmountDue)
    {
        Guid saleGuid = await SetupAgreedSale(saleAgeInDays);

        var putResponse = await buyerClient.PutAsJsonAsync($"publicApi/propertySales/{saleGuid}", new
        {
            Id = saleGuid,
            Status = "cancelled"
        });
        await AssertSuccessAsync(putResponse);

        var modificationResult = await putResponse.Content.ReadFromJsonAsync<PropertySale>();
        Assert.NotNull(modificationResult);
        Assert.Equal("cancelled", modificationResult.Status);
        Assert.Equal(expectedAmountDue, modificationResult.AmountDue);

        await AssertOwnershipUpdate("VENT, Jean", false);
        return saleGuid;
    }

    [Fact]
    public async Task SellerCanResubmitCancelledSale()
    {
        Guid saleGuid = await BuyerCanCancelSaleRightAfterAgreement(20, 0);

        var putResponse = await sellerClient.PostAsync($"publicApi/propertySales/{saleGuid}/resubmit", null);
        await AssertSuccessAsync(putResponse);

        var result = await putResponse.Content.ReadFromJsonAsync<PropertySale>();

        Assert.NotNull(result);
        Assert.NotEqual(result.Id, saleGuid);
        Assert.Equal(propertyAddress, result.Address);
        Assert.Equal(new Price(195000, "EUR"), result.Price);
        Assert.Equal(notaryGuid, result.NotaryId);
        Assert.Null(result.BuyerId);
        Assert.Equal("submitted", result.Status);

        await AssertOwnershipUpdate("VENT, Jean", true);
    }
}