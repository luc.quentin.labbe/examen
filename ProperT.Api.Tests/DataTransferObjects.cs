﻿using ProperT.Core.Domain;

namespace ProperT.Api.Tests;

public record PropertySale(
    Guid Id,
    Guid SellerId,
    Guid? NotaryId,
    Guid? BuyerId,
    string? Address,
    Price? Price,
    LoanRequirements LoanRequirements,
    string Status,
    DateTime? AgreementDate,
    DateTime? RecordingDate,
    DateTime? SigningDate,
    decimal AmountDue);

public record Price(decimal Amount, string Currency);

public record LoanRequirements(decimal Amount, decimal Rate, int DurationInMonths);
