﻿using ProperT.Core.Repositories;

namespace ProperT.Core.Applications;

public class BuyerApplication
{
    private readonly IPropertySaleRepository propertySaleRepository;
    private readonly IOwnershipGatewayService ownershipGatewayService;

    public BuyerApplication(
        IPropertySaleRepository propertySaleRepository,
        IOwnershipGatewayService ownershipGatewayService)
    {
        this.propertySaleRepository = propertySaleRepository;
        this.ownershipGatewayService = ownershipGatewayService;
    }

    public async Task<IReadOnlyPropertySale?> GetPropertySale(Guid currentUserId, Guid saleId)
    {
        return await GetPropertySaleWithAccessRights(currentUserId, saleId);
    }

    private async Task<PropertySale?> GetPropertySaleWithAccessRights(Guid currentUserId, Guid saleId)
    {
        var sale = await propertySaleRepository.GetOrDefault(saleId);
        return sale?.Buyer?.Id == currentUserId
            ? sale
            : null;
    }


    public async Task<IReadOnlyPropertySale?> AddLoadRequirement(IReadOnlyPropertySale sale,
        decimal loanAmount, decimal loanRate, int loanDurationInMonths)
    {
        var mutableSale = (PropertySale)sale;

        mutableSale.LoanRequirements = new LoanRequirements(loanAmount, loanRate, loanDurationInMonths);
        await propertySaleRepository.Save(mutableSale);

        return sale;
    }

    public async Task<IReadOnlyPropertySale> AgreeToPropertySale(IReadOnlyPropertySale sale)
    {
        var mutableSale = (PropertySale)sale;

        mutableSale.RecordAgreement(DateTime.Now);
        await propertySaleRepository.Save(mutableSale);

        return sale;
    }

    public async Task<IReadOnlyPropertySale> CancelPropertySale(IReadOnlyPropertySale sale)
    {
        var mutableSale = (PropertySale)sale;

        mutableSale.Cancel(DateTime.Now);

        await ownershipGatewayService.EnsurePropertyIsOwnedBySeller(mutableSale, ensureNotTransferring: false);
        await ownershipGatewayService.PublishStatusUpdate(mutableSale);

        await propertySaleRepository.Save(mutableSale);

        return mutableSale;
    }
}
