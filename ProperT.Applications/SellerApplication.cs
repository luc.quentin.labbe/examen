﻿using ProperT.Core.Domain.Common;
using ProperT.Core.Repositories;

namespace ProperT.Core.Applications;

public class SellerApplication
{
    private readonly IPropertyRepository propertyRepository;
    private readonly IPropertySaleRepository propertySaleRepository;
    private readonly IIdentityRepository identityRepository;
    private readonly IOwnershipGatewayService ownershipGatewayService;

    public SellerApplication(
        IIdentityRepository identityRepository,
        IPropertyRepository propertyRepository,
        IPropertySaleRepository propertySaleRepository,
        IOwnershipGatewayService ownershipGatewayService)
    {
        this.identityRepository = identityRepository;
        this.propertyRepository = propertyRepository;
        this.propertySaleRepository = propertySaleRepository;
        this.ownershipGatewayService = ownershipGatewayService;
    }

    public async Task<IReadOnlyPropertySale> CreateDraft(Guid currentUserId)
    {
        var sale = new PropertySale(await GetIdentity(currentUserId));

        await propertySaleRepository.Save(sale);

        return sale;
    }

    public async Task<IReadOnlyPropertySale?> GetPropertySale(Guid currentUserId, Guid saleId)
    {
        return await GetPropertySaleWithAccessRights(currentUserId, saleId);
    }

    private async Task<PropertySale?> GetPropertySaleWithAccessRights(Guid currentUserId, Guid saleId)
    {
        var sale = await propertySaleRepository.GetOrDefault(saleId);
        return sale?.Seller.Id == currentUserId
            ? sale
            : null;
    }

    public async Task IdentifyProperty(IReadOnlyPropertySale sale, string address)
    {
        Property property = await FindOrCreateProperty(address);

        var mutableSale = (PropertySale)sale;
        mutableSale.Property = property;
        await propertySaleRepository.Save(mutableSale);
    }

    private async Task<Property> FindOrCreateProperty(string address)
    {
        var property = await propertyRepository.Find(address);
        if (property == null)
        {
            property = new Property(address);
            await propertyRepository.Save(property);
        }

        return property;
    }

    public async Task SetPrice(IReadOnlyPropertySale sale, Price price)
    {
        var mutableSale = (PropertySale)sale;
        mutableSale.Price = price;
        await propertySaleRepository.Save(mutableSale);
    }

    public async Task AssignToNotary(IReadOnlyPropertySale sale, Guid notaryId)
    {
        var mutableSale = (PropertySale)sale;

        mutableSale.Notary = await GetIdentity(notaryId);
        await propertySaleRepository.Save(mutableSale);
    }

    private async Task<Identity> GetIdentity(Guid id)
        => (await identityRepository.GetOrDefault(id))
            ?? throw new InvalidOperationException($"{id} does not exists");

    public async Task Submit(IReadOnlyPropertySale sale)
    {
        var mutableSale = (PropertySale)sale;
        await ownershipGatewayService.EnsurePropertyIsOwnedBySeller(mutableSale, ensureNotTransferring: true);
        mutableSale.Submit();

        await ownershipGatewayService.PublishStatusUpdate(mutableSale);
        await propertySaleRepository.Save(mutableSale);
    }

    public async Task<IReadOnlyPropertySale> SubmitNewSaleFromCancelled(IReadOnlyPropertySale cancelledSale)
    {
        var mutableCancelledSale = (PropertySale)cancelledSale;
        if (mutableCancelledSale.Status != PropertySaleStatus.Cancelled)
            throw new InvalidOperationException($"Sale {mutableCancelledSale.Id} cannot be used to create a new sale "
            + "because the original is not a cancelled sale");

        await ownershipGatewayService.EnsurePropertyIsOwnedBySeller(mutableCancelledSale, ensureNotTransferring: true);

        var sale = new PropertySale(mutableCancelledSale.Seller)
        {
            // Following fields cannot be nulled for a Cancelled sale
            Property = mutableCancelledSale.Property!,
            Price = mutableCancelledSale.Price!,
            Notary = mutableCancelledSale.Notary!
        };
        sale.Submit();

        await ownershipGatewayService.PublishStatusUpdate(sale);
        await propertySaleRepository.Save(sale);

        return sale;
    }

    public async Task ProposeToBuyer(IReadOnlyPropertySale sale, Guid buyerId)
    {
        var mutableSale = (PropertySale)sale;
        mutableSale.LoanRequirements = null;
        mutableSale.Buyer = await GetIdentity(buyerId);
        await propertySaleRepository.Save(mutableSale);
    }
}
